;
; AutoHotkey Version: 1.x
; Language:       English
; Platform:       Win9x/NT
; Author:         A.N.Other <myemail@nowhere.com>
;
; Script Function:
;	Template script (you can customize this template by editing "ShellNew\Template.ahk" in your Windows folder)
;
#SingleInstance off
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
Gui, +LastFound
Gui, Add, Text, x6 y10 w80 h20 , Username:
Gui, Add, Edit, x86 y10 w160 h20 vUsername, 
Gui, Add, Text, x6 y50 w80 h20 Disabled, Full name:
Gui, Add, Text, x6 y70 w80 h20 Disabled, Comment:
Gui, Add, Text, x6 y90 w80 h20 Disabled, User's comment:
Gui, Add, Text, x6 y110 w80 h20 Disabled, Country code:
Gui, Add, Text, x6 y130 w80 h20 Disabled, Account active:
Gui, Add, Text, x6 y150 w80 h20 Disabled, Account expires:
Gui, Add, Text, x246 y50 w90 h20 Disabled, Password last set:
Gui, Add, Text, x246 y70 w90 h20 Disabled, Password expires:
;~ Gui, Add, Text, x246 y90 w110 h20 Disabled, Password changeable:
Gui, Add, Text, x246 y110 w90 h20 Disabled, Password required:
Gui, Add, Text, x246 y130 w90 h20 Disabled, User may change:
Gui, Add, Text, x246 y150 w90 h20 Disabled, Last logon:
Gui, Add, ListView, x6 y180 w490 h170 vGroups, ListView
GuiControl, +List, Groups
;~ ImageListID := IL_Create(2,5,0)
;~ IL_Add(ImageListID, "C:\WINDOWS\system32\shell32.dll", 25)
;~ LV_SetImageList(ImageListID, 1)
Gui, Add, Text, x86 y50 w140 h20 vFullName,
Gui, Add, Text, x86 y70 w140 h20 vComment,
Gui, Add, Text, x86 y90 w140 h20 vUserComment,
Gui, Add, Text, x86 y110 w140 h20 vCountryCode,
Gui, Add, Text, x86 y130 w140 h20 vAccountActive,
Gui, Add, Text, x86 y150 w140 h20 vAccountExpires,
Gui, Add, Text, x356 y50 w140 h20 vPasswordLastSet,
Gui, Add, Text, x356 y70 w140 h20 vPasswordExpires,
Gui, Add, Text, x356 y90 w140 h20 vPasswordExpireNotice, 
Gui, Add, Text, x356 y110 w140 h20 vPasswordRequired,
Gui, Add, Text, x356 y130 w140 h20 vUserMayChange,
Gui, Add, Text, x356 y150 w140 h20 vLastLogon,
Gui, Add, Button, x256 y10 w80 h20 gLookupUser Default, Lookup
Gui, Add, DropDownList, x346 y10 w110 h20 vDDList, View Home Dir
Gui, Add, Button, x466 y10 w30 h20 gDDLGo, Go
; Generated using SmartGUI Creator 4.0
Gui, Show, w503 h358, NetUser
Return

DDLGo:
  GuiControlGet, DDLaction,, DDList
  If DDLaction =
    Return
  GuiControlGet, Username,, Username
  If Username =
	Return
  If DDLaction = View Home Dir
  {
    IfExist, \\bswntp34\prod\home\%username%
      Run, \\bswntp34\prod\home\%username%
    Else
      MsgBox, 16, NetUser, No home directory exists for %username%.
  }
Return

LookupUser:
	Gosub, ClearInfo
	GuiControlGet, Username,, Username
	If Username =
		Return
	outStr := CMDret("net user " . Username . " /domain")
	i = 1
	Loop, Parse, outStr, `n
	{
		If i = 4
		{
			StringReplace, FullName, A_LoopField, Full Name,
			FullName = %FullName%
			GuiControl,,FullName, %FullName%
		}
		If i = 5
		{
			StringReplace, Comment, A_LoopField, Comment,
			Comment = %Comment%
			GuiControl,,Comment, %Comment%
		}
		If i = 6
		{
			StringReplace, UserComment, A_LoopField, User's comment,
			UserComment = %UserComment%
			GuiControl,,UserComment, %UserComment%
		}
		If i = 7
		{
			StringReplace, CountryCode, A_LoopField, Country code,
			CountryCode = %CountryCode%
			GuiControl,,CountryCode, %CountryCode%
		}
		If i = 8
		{
			StringReplace, AccountActive, A_LoopField, Account active,
			AccountActive = %AccountActive%
			GuiControl,,AccountActive, %AccountActive%
			IfInString, AccountActive, No
			{
				Gui, Font, cRed Bold
				GuiControl, Font, FullName
				GuiControl, Font, Comment
				GuiControl, Font, AccountActive
			}
		}
		If i = 9
		{
			StringReplace, AccountExpires, A_LoopField, Account expires,
			AccountExpires = %AccountExpires%
			GuiControl,,AccountExpires, %AccountExpires%
		}
		If i = 11
		{
			StringReplace, PasswordLastSet, A_LoopField, Password last set,
			PasswordLastSet = %PasswordLastSet%
			GuiControl,,PasswordLastSet, %PasswordLastSet%
		}
		If i = 12
		{
			StringReplace, PasswordExpires, A_LoopField, Password expires,
			PasswordExpires = %PasswordExpires%
			GuiControl,,PasswordExpires, %PasswordExpires%
			ExpireSpace := InStr(PasswordExpires, A_Space)
			ExpireDate := SubStr(PasswordExpires, 1, ExpireSpace)
			StringSplit, Expire, ExpireDate, /
			If StrLen(Expire1) = 1
				Expire1 := "0" . Expire1
			If StrLen(Expire2) = 1
				Expire2 := "0" . Expire2
			ExpireDate := Expire3 Expire1 Expire2
			StringReplace, ExpireDate, ExpireDate, %A_Space%,,All
			FormatTime, Today,, yyyyMMdd
			EnvSub, ExpireDate, %Today%, days
			GuiControl,, PasswordExpireNotice, Expires in %ExpireDate% days
			If ExpireDate < 7
				Gui, Font, cMaroon Bold
			If ExpireDate <= 2
				Gui, Font, cRed Bold
			GuiControl, Font, PasswordExpireNotice  ; Put the above font into effect for a control.
		}
		If i = 13
		{
			StringReplace, PasswordChangeable, A_LoopField, Password changeable,
			PasswordChangeable = %PasswordChangeable%
			GuiControl,,PasswordChangeable, %PasswordChangeable%
		}
		If i = 14
		{
			StringReplace, PasswordRequired, A_LoopField, Password required,
			PasswordRequired = %PasswordRequired%
			GuiControl,,PasswordRequired, %PasswordRequired%
		}
		If i = 15
		{
			StringReplace, UserMayChange, A_LoopField, User may change password,
			UserMayChange = %UserMayChange%
			GuiControl,,UserMayChange, %UserMayChange%
		}
		If i = 21
		{
			StringReplace, LastLogon, A_LoopField, Last logon,
			LastLogon = %LastLogon%
			GuiControl,,LastLogon, %LastLogon%
		}
		If i >= 25
		{
			Groups = %Groups% %A_LoopField%
		}
	i++
	}
	StringReplace, Groups, Groups, The command completed successfully.,
	StringReplace, Groups, Groups, `r,,All
	StringReplace, Groups, Groups, %A_Tab%,,All
	StringReplace, Groups, Groups, %A_Space%,,All
	Groups = %Groups%
;~ 	ClipBoard = %Groups%
	StringGetPos, LocalGroupsEndPos, Groups, GlobalGroupmemberships
	LocalGroups := SubStr(Groups, 1, LocalGroupsEndPos)
	StringReplace, Groups, Groups, %LocalGroups%,,All
	StringReplace, LocalGroups, LocalGroups, LocalGroupMemberships,
	StringReplace, Groups, Groups, GlobalGroupmemberships,
;~ 	LVA_ListViewAdd("Groups")
	Loop, Parse, LocalGroups, *
	{
		If A_LoopField =
			Continue
		LV_Add("",A_LoopField)
;~ 		LVA_SetCell("Groups", A_Index, 1, "red")
	}
	
	Loop, Parse, Groups, *
	{
		If A_LoopField =
			Continue
		LV_Add("",A_LoopField)
	}
	
;~ OnMessage("0x4E", "LVA_OnNotify")
;~ LVA_Refresh("Groups")
Return

ClearInfo:
	GuiControl,, FullName,
	GuiControl,, Comment,
	GuiControl,, UserComment,
	GuiControl,, CountryCode,
	GuiControl,, AccountActive,
	GuiControl,, AccountExpires,
	GuiControl,, PasswordLastSet,
	GuiControl,, PasswordExpires,
	GuiControl,, PasswordExpireNotice,
	GuiControl,, PasswordRequired,
	GuiControl,, UserMayChange,
	GuiControl,, LastLogon,
    LV_Delete()
    Groups =
    LocalGroups =
	Gui, Font, cBlack Norm
	GuiControl, Font, FullName
	GuiControl, Font, Comment
	GuiControl, Font, AccountActive
	GuiControl, Font, PasswordExpireNotice
Return

F10::ListVars

GuiClose:
GuiEscape:
ExitApp


;~ http://www.autohotkey.com/forum/topic3687.html
CMDret(CMD)
{
  VarSetCapacity(StrOut, 10000)
  RetVal := DllCall("cmdret.dll\RunReturn", "str", CMD, "str", StrOut)
  Return, %StrOut%
}